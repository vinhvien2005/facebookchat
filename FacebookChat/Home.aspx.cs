﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace FacebookChat
{
    public partial class Home : System.Web.UI.Page
    {

        private string acessURL = "https://graph.facebook.com/v2.6/me/messages?access_token={0}";
        private string sendData = "{\"recipient\":{\"id\":\"{id}\"},\"message\":{\"text\":\"{messages}\"}}";
        public List<User> userList = new List<User>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string data = txtSelectedUser.Value;
                LoadUserData();
                lblError.InnerText = string.Empty;
                txtSelectedUser.Value = string.Empty;
                if (IsPostBack && !string.IsNullOrEmpty(data))
                {
                    lblError.InnerText = ExcuteSendMessages(data);
                }
            }
            catch (Exception ex)
            {
                lblError.InnerText = ex.Message;
            }
        }
        private string ExcuteSendMessages(string data)
        {
            // send messages !
            string[] splitData = data.Split(',');
            string returnMessages = "Send successed !";
            for (int i = 0; i < splitData.Length; i++)
            {
                if (!string.IsNullOrEmpty(PostMessage(splitData[i], txtMessages.Value)))
                {
                    returnMessages += string.Format("userid = {0} send messages doesn't successed ! \n", splitData[i]);
                }
            }
            return returnMessages;
        }

        private string PostMessage(string id, string messages)
        {
            string result = string.Empty;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format(acessURL, System.Configuration.ConfigurationSettings.AppSettings["Acesstoken"].ToString()));
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = sendData.Replace("{id}", id); //string.Format(sendData,id,messages);
                    json = json.Replace("{messages}", messages);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    messages = "false";
                }
            }
            catch (Exception ex)
            {
                // log error 
                messages = "false";
            }
            return result;
        }

        private void LoadUserData()
        {
            string pathDataFile = System.Configuration.ConfigurationSettings.AppSettings["DataFile"].ToString();
            using(StreamReader sr = new StreamReader(pathDataFile))
            {
                string lineData= string.Empty;
                while ((lineData = sr.ReadLine()) != null)
                {
                    if (!string.IsNullOrEmpty(lineData))
                    {
                        string[] data = lineData.Split('#');
                        if (data.Length == 3)
                        {
                            userList.Add(new User() { Id = data[0], Name = data[1],Image=data[2] });
                        }
                        else
                        {
                            throw new Exception("data file format is wrong !");
                        }
                    }
                }
            }
        }


    }
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public User()
        {
            Id = string.Empty;
            Name = string.Empty;
            Image = string.Empty;
        }
    }
}