﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="FacebookChat.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <label runat="server" id="lblError" style="color:red;font-size:15px"></label><br />
    <label> User List : </label>
    <table>
        <tbody>
            <tr>
                <td><input  type="checkbox" id="chkall" onclick="checkAll();"/>
                </td>
                <td>Select all</td>
            </tr>
            <%if(userList.Count > 0) {%>
                <%for(int i = 0;i<userList.Count;i++) {%>
                    <tr>
                        <td><input  type="checkbox" class="data" value="<%=userList[i].Id %>"/>
                        </td>
                        <td><%=userList[i].Name %></td>
                        <td><image style="width:50px;height:50px" src="<%=userList[i].Image %>"></image></td>
                    </tr>
                
            <%}} %>
        </tbody>
    </table>
    <form id="myform" runat="server">
        <input type="hidden" id="txtSelectedUser" runat="server" />
    <div>
        Message content :
        <table>
            <tbody>
                <tr>
                    <td>
                        <textarea runat="server" id="txtMessages" style="width:200px;height:200px;"></textarea>
                    </td>
                </tr>
                <tr>
                    <td> 
                        <input type="button" onclick="submitForm();"  value="submit"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
    
    <script type="text/ecmascript">
        function submitForm()
        {
            // check have checked user 
            var listUser = Array();
            var txtMessages = document.getElementById("txtMessages");
            if (hasChecked() && txtMessages.value.length > 0) {
                var allCheckbox = document.getElementsByClassName("data");
                for (var i = 0; i < allCheckbox.length; i++)
                {
                    if (allCheckbox[i].checked)
                    {
                        listUser.push(allCheckbox[i].attributes["value"].value);
                    }
                }
                document.getElementById("txtSelectedUser").value = listUser.toString();
                document.getElementById("myform").submit();
            }
            else {
                alert("please check user and submit again !");
            }
            
        }
        function hasChecked()
        {
            var allCheckbox = document.getElementsByClassName("data");
            for (var i = 0; i < allCheckbox.length; i++)
            {
                if (allCheckbox[i].checked)
                {
                    return true;
                }
            }
            return false;
        }
        function checkAll()
        {
            var checkall = document.getElementById("chkall");
            var allCheckbox = document.getElementsByClassName("data");
            if (allCheckbox.length > 0)
            {
                for (var i = 0; i < allCheckbox.length; i++)
                {
                    allCheckbox[i].checked = checkall.checked;
                }
            }
        }
    </script>
</html>
